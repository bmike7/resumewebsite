module.exports = {
  pathPrefix: "/resumewebsite",
  siteMetadata: {
    sitePrefix: "Mike Bijl",
    pages: [
      {
        name: "Home",
        title: "Web Developer",
        description: `Personal/Resumé website.
          You can also find (a) blog(s) about what I am building and learning`,
      },
      {
        name: "404",
        title: "Not Found",
        description: "I'm sorry, seems like the page you requested doesn't exist",
      },
    ],
  },
  plugins: [
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    "gatsby-plugin-react-helmet", 
    "gatsby-theme-material-ui",
    "gatsby-plugin-svgr"
  ],
};
