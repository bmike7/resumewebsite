import React from "react";
import { graphql } from "gatsby";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import Layout from "../components/Layout";
import SEO from "../components/SEO";

const useStyles = makeStyles(
  createStyles({
    root: {
      width: "90%",
      maxWidth: 850,
      margin: "auto",
    }
  })
);

const BlogPost = ({data}) => {
    const classes = useStyles();
    const post = data.markdownRemark;

    return (
    <Layout>
        <SEO title={post.frontmatter.title} description={post.excerpt}/>
        <div className={classes.root}>
            <div dangerouslySetInnerHTML={{ __html: post.html }} />
        </div>
    </Layout>
)};

export default BlogPost;

export const query = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        sitePrefix
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
      excerpt
    }
  }
`;
