import React, { HTMLAttributes } from "react";
import { Typography } from "@material-ui/core";
import {
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core/styles";
import { Button } from "gatsby-theme-material-ui";

import NotFound from "../assets/NotFound";
import PageContainer from "../components/PageContainer";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        center: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            textAlign: 'center',
            width: '40%',
            [theme.breakpoints.down('md')]: { width: '50%' },
            [theme.breakpoints.down('sm')]: { width: '60%' },
        },
        title: { 
            fontSize: '3.5rem',
            marginTop: '-4%',
            textTransform: 'uppercase',
        },
        button: {
            padding: theme.spacing(.5, 3),
            marginTop: theme.spacing(2),
        }
    })
);

const PageNotFound: React.FC<HTMLAttributes<HTMLDivElement>> = () => {
    const classes = useStyles();
    return (
        <PageContainer seo={{ pageName: "404"}} >
            <div className={classes.center}>
                <NotFound />
                <Typography variant="h1" className={classes.title}>Sorry, didn't found<br/>what you are looking for... </Typography>
                <Button className={classes.button}
                    color="primary" 
                    variant="contained" 
                    to="/">
                        Home
                </Button>
            </div>
        </PageContainer>
)};
export default PageNotFound;
