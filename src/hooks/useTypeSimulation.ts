import React, { useState } from "react";

const asyncDelay = (delay: number): Promise<void> =>
    new Promise(res => setTimeout(res, delay));

const simulateTyping = async (
    sentence: string,
    setStateDispatcher: React.Dispatch<React.SetStateAction<string>>,
) => {
    let terminalString = "$ ";
    setStateDispatcher(terminalString);
    for (const char of sentence) {
        terminalString += char;
        setStateDispatcher(terminalString);
        await asyncDelay(50);
    }
    await asyncDelay(400);
    setStateDispatcher("");
};

const useTypeSimulation = (): [string, (sentence: string) => Promise<void>] => {
    const [typedText, setTypedText] = useState<string>("");

    const startTyping = async (sentence: string) => 
        simulateTyping(sentence, setTypedText);

    return [typedText, startTyping];
}

export default useTypeSimulation;
