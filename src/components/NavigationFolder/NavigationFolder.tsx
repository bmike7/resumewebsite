import React from "react";
import clsx from "clsx";
import { createStyles, makeStyles, Theme, darken } from "@material-ui/core/styles";

import Folder from "@material-ui/icons/FolderOpen";
import { Typography } from "@material-ui/core";
import { getOwnPropertySymbols } from "core-js/fn/object";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: { 
            textAlign: 'center',
            width: '25%',
            minWidth: 75,
            margin: theme.spacing(1, 1, 0),
            color: theme.palette.background.default,
            cursor: 'pointer',
        },
        folder: { 
            width: '80%',
            height: '80%',
        },
        folderName: {
            marginTop: '-15%',
            textTransform: 'uppercase',
            fontWeight: 600,
        },
        active: { 
            color: darken(theme.palette.primary.main, .8),
            pointerEvents: 'none',
            opacity: .65,
        },
    })
);

export interface NavigationFolderProps {
    className?: HTMLDivElement["className"],
    name: string,
    path: string,
    handleClick: (path: string) => void,
    isActive?: boolean,
}

const NavigationFolder: React.FC<NavigationFolderProps> = props => {
    const classes = useStyles();
    const {
        className, 
        name, 
        path, 
        handleClick,
        isActive = false,
    } = props;

    return (
    <div className={clsx(classes.root, className, {[classes.active]: isActive})} 
        onClick={() => handleClick(path)}
    >
        <Folder className={classes.folder} />
        <Typography className={classes.folderName} variant="subtitle1">{name}</Typography>
    </div>
)};

export default NavigationFolder;
