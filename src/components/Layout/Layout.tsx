import React, { useState } from "react";
import clsx from "clsx";
import { CssBaseline } from "@material-ui/core";
import { createStyles, makeStyles, Theme, lighten } from "@material-ui/core/styles";

import Menu from "@material-ui/icons/MenuRounded";
import NavigationModal from "../NavigationModal";
import Footer from "../Footer";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: { position: "relative" },
        center: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
        },
        header: {
            width: '100%',
            display: 'flex',
            padding: theme.spacing(4, 5, 0, 8),
            [theme.breakpoints.down("xs")]: {
                padding: theme.spacing(3, 3, 0),
            }
        },
        menuIcon: { 
            marginLeft: 'auto', 
            cursor: 'pointer',
        },
        navigationModal: {
            width: '50vw',
            height: '40%',
            backgroundColor: lighten(theme.palette.background.paper, .8),
            outline: 'none',
            borderRadius: '5px',
            overflow: 'hidden',
            boxShadow: theme.shadows[20],
        },
        footer: {
            position: "relative",
            bottom: 0,
        }
    })
);

const Layout: React.FC = ({ children }) => {
    const classes = useStyles();
    const [open, setOpen] = useState<boolean>(false);

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div className={classes.root}>
            <CssBaseline />
            <div className={classes.header}>
                <div onClick={handleOpen} className={classes.menuIcon}>
                    <Menu fontSize="large" />
                </div>
                <NavigationModal 
                    className={clsx(classes.navigationModal, classes.center)}
                    modalProps={{ open, onClose: handleClose }}/>
            </div>
            {children}
            <Footer className={classes.footer}/>
        </div>
    )
}

export default Layout;
