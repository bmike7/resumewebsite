import { navigate } from "gatsby";
import { DeveloperAction } from "./Developer";

const openLinkedIn: DeveloperAction = {
    command: "open",
    path: "linkedIn/mikebijl",
    action: () => window.open("https://www.linkedin.com/in/mike-bijl-b3459519a/", "_blank"),
};
const openResume: DeveloperAction = {
    command: "wget",
    path: "CVMikeBijl.pdf",
    action: () => alert("should download/open CV"),
};
const openSourceCode: DeveloperAction = {
    command: "open",
    path: "gitlab/bmike7/resume",
    action: () => window.open("https://gitlab.com/bmike7/resumewebsite", "_blank"),
};
const openNotFound: DeveloperAction = {
    command: "cd",
    path: "blogs/themostawemsomestblogpost",
    action: () => navigate("/blogs/themostawemsomestblogpost"),
}

export {
    openLinkedIn,
    openResume,
    openSourceCode,
    openNotFound,
};
