import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import BlogCard, { Blog } from "../BlogCard";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        cardContainer: {
            display: "flex",
            flexWrap: "wrap",
            width: "90%",
            margin: "auto",
            marginTop: theme.spacing(6),
            justifyContent: "center",
        },
    })
);

interface GraphData {
    allMarkdownRemark: {
        edges: {
            node: Blog
        }[],
    }
}

const Blogposts: React.FC = () => {
    const classes = useStyles();

    const data = useStaticQuery<GraphData>(query);
    const blogposts = data.allMarkdownRemark.edges;

    return (
        <section className={classes.cardContainer}>
            {blogposts.map(({ node: blog} ) => 
                <BlogCard key={blog.id} blog={blog}/>)
            }
        </section>
    );
};
export default Blogposts;

const query = graphql`
    query {
        allMarkdownRemark {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        description
                        date
                        url
                    }
                    fields {
                        slug
                    }
                }
            }
          }
    }
`;
