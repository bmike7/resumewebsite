import React from "react";
import Helmet, { HelmetProps } from "react-helmet";
import { useStaticQuery, graphql } from "gatsby";

/* I wanted that helmet-related stuff would happen automatically
 * for most of the pages (can be overwritten by a nested helmet component -> ex: WIP).
 * GraphQL data handling that comes by default with Gatsby
 * could be used for this.
*/

interface metaProps {
    name: string,
    content: string,
}

export interface SEOProps extends Omit<HelmetProps, "meta"> {
    pageName: string,
    description?: string,
    lang?: string,
    meta?: metaProps[]
}

/** For now i'm okay with this component but needs to be redone
 * in the future.
 * TODO: customize the schema with a resolver to query on pages
 * to find a specific one.
 * Why I didn't do it now:
 * First time using Graphql and the documentation was't that
 * clear for me about the customization ...
 * Didn't want to waste to much time on figuring this out,
 * first get a first version online was my priority.
 */
const SEO: React.FC<SEOProps> = props => {
    const { 
        pageName,
        title: propsTitle,
        description: propsDescription,
        meta: propsMeta = [],
        lang = "en", 
    } = props;

    const data = useStaticQuery<GraphData>(query);
    const page = data.site.siteMetadata.pages.find(page => page.name == pageName);

    const title = propsTitle || page?.title || "";
    const description = propsDescription || page?.description || "";

    return (
    <Helmet 
        title={data.site.siteMetadata.sitePrefix}
        titleTemplate={`%s | ${title}`}
        htmlAttributes={{ lang, }}
        meta={[
            {
                name: "description",
                content: description,
            },
            {
                name: "keywords",
                content: "Web Developer, Software Engineer, Web Design, Logo Design, SVG Drawings"
            },
            ...propsMeta
        ]}
    />
)};
export default SEO;

interface Page {
    name: string,
    title: string,
    description: string,
}

interface GraphData {
    site: {
        siteMetadata: {
            sitePrefix: string,
            pages: Page[],
        }
    }
}

const query = graphql`
    query {
        site {
            siteMetadata {
                sitePrefix
                pages {
                    name
                    title
                    description
                }
            }
        }
    }
`;
