import React, { HTMLAttributes } from "react";

import Technology from '../../assets/Technology';

const TechnologySVG: React.FC<HTMLAttributes<HTMLDivElement>> = ({ ...props }) => (
    <div {...props}>
        <Technology />
    </div>
);
export default TechnologySVG;
