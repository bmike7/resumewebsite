import React from "react";
import { Button } from "gatsby-theme-material-ui";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

export default {
    title: "Static Web Site/Buttons",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        block: {
            margin: theme.spacing(1),
            display: "block",
        },
    }),
);

export const Buttons = () => {
    const classes = useStyles();

    return (
        <>
            <Button className={classes.block} color="primary">Primary Text Button</Button>
            <Button className={classes.block} color="secondary" size="small" >Secondary Text Button</Button>
            <Button className={classes.block} variant="contained" color="primary">PRIMARY CONTAINED BUTTON</Button>
            <Button className={classes.block} variant="contained" color="secondary">SECONDARY CONTAINED BUTTON</Button>
        </>
    )
};
